<div>
    <div class="container">
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-12 mt-5">
                    <img src="/images/odfotografie_portfolio_5.jpg" class="img-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-3 text-center">
                            <div class="card bg-dark"style="border-radius:0px; margin-top: -600px; margin-right: -40px;">
                                <div class="card-body text-light h4">
                                    <b>Impressum</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right: -40px;">
                                <div class="card-body text-light h4">
                                    <b>& Rechtliches</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right: -40px;">
                                <div class="card-body text-light h4">
                                    <b>von Rahel & Ron</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-12 mt-5">
                    <img src="/images/odfotografie_portfolio_5.jpg" class="img-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-2 text-center">
                            <div class="card bg-dark"style="border-radius:0px; margin-top: -0px; margin-right: -px;">
                                <div class="card-body text-light h4">
                                    <b>Impressum</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right: -px;">
                                <div class="card-body text-light h4">
                                    <b>& Rechtliches</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right: -px;">
                                <div class="card-body text-light h4">
                                    <b>von Rahel & Ron</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1"></div>
            <div class="col-lg-11 text-start mt-5">
                <p class="text-dark h3">
                    BETREIBER, KONZEPT & GESTALTUNG
                </p>
                <p class="text-dark h5 mt-4">
                    <b>Rahel Rüttimann & Ron Edwards</b>
                </p>
                <p class="text-dark h5 mt-4">
                    <b>Adresse:</b>
                </p>
                <p class="text-dark h5">
                    Denkmalstrasse 17
                </p>
                <p class="text-dark h5">
                    6006 Luzern
                </p>
                <p class="text-dark h5">
                    Schweiz
                </p>
                <p class="text-dark h5 mt-4">
                    <b>Email:</b> hello@rahelandron.ch
                </p>
                <p class="text-dark h5">
                    <b>Telefon:</b> +41 79 961 48 85 
                </p>
                <p class="text-dark h1 mt-5">
                    HAFTUNGSAUSSCHLUSS
                </p>
                <p class="text-dark h5 mt-4">
                    Der Autor übernimmt keinerlei Gewähr hinsichtlich der inhaltlichen Richtigkeit, Genauigkeit, Aktualität, Zuverlässigkeit und 
                </p>
                <p class="text-dark h5">
                    Vollständigkeit der Informationen. Haftungsansprüche gegen den Autor wegen Schäden materieller oder immaterieller Art, 
                </p>
                <p class="text-dark h5">
                    welche aus dem Zugriff oder der Nutzung bzw. Nichtnutzung der veröffentlichten Informationen, durch Missbrauch der 
                </p>
                <p class="text-dark h5">
                    Verbindung oder durch technische Störungen entstanden sind, werden ausgeschlossen. Alle Angebote sind unverbindlich.
                </p>
                <p class="text-dark h5 mt-4">
                    Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu 
                </p>
                <p class="text-dark h5">
                    verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen. 
                </p>
                <p class="text-dark h5 mt-4">
                    Verweise und Links auf Webseiten Dritter liegen ausserhalb unseres Verantwortungsbereichs. Es wird jegliche Verantwortung 
                </p>
                <p class="text-dark h5">
                    für solche Webseiten abgelehnt. Der Zugriff und die Nutzung solcher Webseiten erfolgen auf eigene Gefahr des Nutzers oder 
                </p>
                <p class="text-dark h5">
                    der Nutzerin.
                </p>
                <p class="text-dark h1 mt-5">
                    URHEBERRECHTE
                </p>
                <p class="text-dark h5 mt-4">
                    Die Urheber- und alle anderen Rechte an Inhalten, Bildern, Fotos oder anderen Dateien auf der Website gehören 
                </p>
                <p class="text-dark h5">
                    ausschliesslich Rahel & Ron (rahelandron.ch) oder den speziell genannten Rechtsinhabern. Für die Reproduktion jeglicher 
                </p>
                <p class="text-dark h5">
                    Elemente (alle Inhalt dieses Blogs, Fotos sowie auch Texte) ist die schriftliche Zustimmung der Urheberrechtsträger im Voraus  
                </p>
                <p class="text-dark h5">
                    einzuholen.
                </p>
                <p class="text-dark h5 mt-4">
                    Falls eine Publikation ohne die Zustimmung von Rahel & Ron (rahelandron.ch) erfolgt, können und werden rechtliche Schritte 
                </p>
                <p class="text-dark h5">
                  eingeleitet. Bitte benutzen Sie für ein Gesuch zur Reproduktion das unten aufgeführten Link zum Kontaktformular.  
                </p>
            </div>
        </div>
    </div>
</div>
