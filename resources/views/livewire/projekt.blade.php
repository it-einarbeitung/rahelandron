<div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-12 mt-5">
                <img src="/images/2113_rahelandron-feuerring-08174.jpg" class="img-fluid">
                <div class="row">
                    <div class="col-lg-9">
                    </div>
                    <div class="col-lg-3 text-center">
                        <div class="card bg-dark"style="border-radius:0px; margin-top: -700px; margin-right: -40px;">
                            <div class="card-body text-light h4">
                                <b>Mit Rahel & Ron zu</b>
                            </div>
                        </div>
                        <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right: -40px;">
                            <div class="card-body text-light h4">
                                <b>Besuch bei Feuerring</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="row mt-5">
            <div class="col-lg-1"></div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3">
                    Seit 2017 fotografieren wir Food, Restaurants und 
                </p>
                <p class="text-dark h3">
                    Unternehmen in der Gastronomie & Lebensmittel-Branche.
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6">
            </div>
            <div class="col-lg-5 text-center">
                <div class="card bg-dark"style="border-radius:0px; ">
                    <div class="card-body text-light h4">
                        <b>EINE AUSWAHL UNSERER</b>
                    </div>
                </div>
                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; ">
                    <div class="card-body text-light h4">
                        <b>FOTOGRAFIE ARBEITEN</b>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 mt-5">
                <img src="/images/dron_bäckerforum_aeschlimann_foodfotografie_5.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b class="text-light">Food Fotografie für<br></b>
                    <b>Restaurant-Eröffung</b>  
                </p>
                <p class="text-secondary text-start">
                    BÄCKERFORUM AESCHLIMANN
                </p>
            </div>
            <div class="col-lg-5">
                <img src="/images/rahelandron-foodfotografie-01089.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b class="text-light">Food Fotografie für<br></b>
                    <b>«Die Hühnerei»</b>  
                </p>
                <p class="text-secondary text-start">
                    CHESA ROSATSCH
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 text-center">
                <img src="/images/49_heimatli-rahelandron-foodfotografie-00750-p-1600.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b class="text-light">Food Fotografie<br></b>
                    <b>für's «Heimatli»</b>  
                </p>
                <p class="text-secondary text-start">
                    CHESA ROSATSCH
                </p>
            </div>
            <div class="col-lg-6 text-center">
                <img src="/images/6067094273adcbbc451cb707_uondas-rahelandron-foodfotografie-00110-p-1600.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b class="text-dark">
                        <b class="text-light">Food Fotografie für<br></b>
                        <b>das «Uondas»</b>  
                </p>
                <p class="text-secondary text-start">
                    CHESA ROSATSCH CELERINA
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 text-center mt-5">
                <img src="/images/5c5e092a364f64816ef53ff3_rahelandron_restaurant_schlüssel_restaurantfotografie_3.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b class="text-light">Restaurant<br></b>
                    <b>Fotografie Sterne-Ambiente</b>  
                </p>
                <p class="text-secondary text-start">
                    RESTAURANT SCHLÜSSEL, OBERWIL
                </p>
            </div>
            <div class="col-lg-5 text-center">
                <img src="/images/rahelandron_restaurant_schlüssel_foodfotografie_restaurantfotografie_14.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b class="text-dark">
                        <b class="text-light">Food Fotografie für<br></b>
                        <b>Sterne Menü</b>  
                </p>
                <p class="text-secondary text-start">
                    RESTAURANT SCHLÜSSEL, OBERWIL
                </p> 
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6 text-center ">
                <img src="/images/uh-baerlauchwurst-packshot_hurrah_luzern_08-p-1600.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b>Produkt Fotografie<br> 
                        für Bio-Metzgerei</b>  
                </p>
                <p class="text-secondary text-start">
                    UELI-HOF BIO-METZGEREI
                </p>
            </div>
            <div class="col-lg-6 text-center mt-5">
                <img src="/images/ueli-hof-produkt-fotografie-rahelandron-natur-02778-p-1600.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b class="text-light">Key Visuals für neue</b> <br>
                        <b>Verpackungen</b>  
                </p>
                <p class="text-secondary text-start">
                    UELI-HOF BIO-METZGEREI
                </p>
            </div>   
        </div>
        <div class="row mt-5">
            <div class="col-lg-4">
            </div>
            <div class="col-lg-4 text-center mt-5">
                <img src="/images/171e43_DSC02109.jpg" class="img-fluid">
                <p class="text-dark h3 text-start" style="margin-top: -30px;">
                    <b class="text-light">Bio-Bauernhof Tier</b> <br>
                        <b>Fotografie</b>  
                </p>
                <p class="text-secondary text-start">
                    UELIHOF FLEISCHMANUFAKTUR
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 text-center mt-5">
                <hr class="bg-dark">
                <p class="text-dark h2 mt-5">
                    Hunger auf leckere Foodfotografie ? Lass uns sprechen.
                </p>
                <button class="btn btn-outline-dark border border-dark text-muted mt-5"style="border-radius: 0px; height: 70px; width: 200px;">
                        MELDE  DICH  BEI  UNS !
                </button>
            </div>
        </div>
    </div>
</div>

