<div class="bg-white">
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-2"></div>
            <div class="col-lg-10 text-start mt-5">
                <p class="text-dark h1 mt-5">
                    Wir lieben Food-Fotografie – genau so wie du. Hier 
                </p>
                <p class="text-dark h1">
                    zeigen wir dir unsere Lieblinge der letzten Monate.
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-8">
            </div>
            <div class="col-lg-4 text-center mt-5">
                <div class="card bg-dark"style="border-radius:0px; ">
                    <div class="card-body text-light h4">
                        <b>UNSERE</b>
                    </div>
                </div>
                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; ">
                    <div class="card-body text-light h4">
                        <b>FOOD-FOTOGRAFIE</b>
                    </div>
                </div>
                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; ">
                    <div class="card-body text-light h4">
                        <b>HIGHLIGHTS</b>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-2 mt-5 text-center">
                <a href="#" class="nav-link text-dark h4">
                    Food
                </a>
            </div>
            <div class="col-lg-2 text-center mt-5">
                <a href="#" class="nav-link text-dark h4">
                    Gastronomie
                </a>
            </div>
            <div class="col-lg-2 text-center mt-5">
                <a href="#" class="nav-link text-dark h4">
                    Business
                </a>
            </div>
            <div class="col-lg-2 text-center mt-5">
                <a href="#" class="nav-link text-dark h4">
                    Events
                </a>
            </div>
            <div class="col-lg-2 text-center mt-5">
                <a href="#" class="nav-link text-dark h4">
                    Landwirtschaft
                </a>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center">
                <img src="/images/69_rahelandron_foodfotografie_portfolio_18.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    IRISH-COFFEE MILKSHAKE
                </p>
            </div>
            <div class="col-lg-1">
            </div>
            <div class="col-lg-5 text-center mt-5">
                <img src="/images/helandron_foodfotografie_portfolio_12.jpg" class="img-fluid mt-5">
                <p class="text-secondary text-start mt-3">
                    BURGER STILLLEBEN
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center mt-5">
                <img src="/images/on_foodfotografie_portfolio_14.jpg" class="img-fluid mt-5">
                <p class="text-secondary text-start mt-3">
                    ONION RINGS – COLOR STILLLIFE FOOD
                </p>
            </div>
            <div class="col-lg-1">
            </div>
            <div class="col-lg-5 text-center">
                <img src="/images/ae_rahelandron_foodfotografie_portfolio_19.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    MILKSHAKE EXPLOSION – COLOR STILLLIFE
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center mt-5">
                <img src="/images/fotografie_portfolio_16.jpg" class="img-fluid mt-5">
                <p class="text-secondary text-start mt-3">
                    DROPPED MEXICAN POP CORN
                </p>
            </div>
            <div class="col-lg-1">
            </div>
            <div class="col-lg-5 text-center">
                <img src="/images/ron_foodfotografie_portfolio_17.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                   MINIMALST POP CORN – COLOR STILLLIFE
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center">
                <img src="/images/odfotografie_portfolio_22.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    RANDENKNÖDEL
                </p>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <img src="/images/otografie_portfolio_30.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    MINIMALIST GUA-BAO
                </p>
                <a href="#" class="text-dark mt-5 text-start">
                    GUA BAO BURGER
                </a>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center">
                <img src="/images/ografie_portfolio_20.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    MEHL WÜRFEL DESTRUKTION
                </p>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <img src="/images/on_foodfotografie_portfolio_24.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    MEHL HÄNDE
                </p>
                <a href="#" class="text-dark mt-5 text-start">
                    GUA BAO BURGER
                </a>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center">
                <img src="/images/odfotografie_portfolio_25.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    TEIG HÄNDE
                </p>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <img src="/images/on_foodfotografie_portfolio_26.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                   BROT HÄNDE
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center">
                <img src="/images/ografie_portfolio_29.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    PAPAYE FRUCHT FOTOGRAFIE
                </p>
            </div>
            <div class="col-lg-1">
            </div>
            <div class="col-lg-5 text-center mt-5">
                <img src="/images/tografie_portfolio_4.jpg" class="img-fluid mt-5">
                <p class="text-secondary text-start mt-3">
                    HOT DOG FAST-FOOD FOTOGRAFIE
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center">
                <img src="/images/rafie_portfolio_11.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    TOMATO MINIMALIST FOOD PHOTOGRAPHY
                </p>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <img src="/images/tfolio_6.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                   PHOTO SCHWEIZ 19
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center">
                <img src="/images/afie_portfolio_1.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    PHOTO SCHWEIZ 19
                </p>
            </div>
            <div class="col-lg-1">
            </div>
            <div class="col-lg-5 text-center mt-5">
                <img src="/images/rafie_portfolio_7.jpg" class="img-fluid mt-5">
                <p class="text-secondary text-start mt-3">
                    PHOTO SCHWEIZ 19
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 text-center">
                <img src="/images/rtfolio_10.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    SORBET FOOD PHOTOGRAPHY
                </p>
            </div>
            <div class="col-lg-1">
            </div>
            <div class="col-lg-5 text-center mt-5">
                <img src="/images/portfolio_5.jpg" class="img-fluid mt-5">
                <p class="text-secondary text-start mt-3">
                    FAST-FOOD FOTOGRAFIE
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5 ">
                <img src="/images/schlimann_foodfotografie_5.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    BROT FOTOGRAFIE FÜR ERÖFFNUNGSPLAKATE
                </p>
                <a href="#" class="text-dark mt-5 text-start">
                   FOODFOTOGRAFIE FÜR RESTAURANT-ERÖFFNUNG
                </a>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <img src="/images/hlimann_foodfotografie_6.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    BROT FOTOGRAFIE FÜR ERÖFFNUNGSPLAKATE
                </p>
                <a href="#" class="text-dark mt-5 text-start">
                    FOODFOTOGRAFIE FÜR RESTAURANT-ERÖFFNUNG
                </a>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5">
                <img src="/images/imann_foodfotografie_8.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    FOOD FOTOGRAFIE FÜR ERÖFFNUNGSPLAKATE
                </p>
                <a href="#" class="text-dark mt-5 text-start">
                   FOODFOTOGRAFIE FÜR RESTAURANT-ERÖFFNUNG
                </a>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <img src="/images/chlimann_foodfotografie_14.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    PIZZA FOTOGRAFIE FÜR ERÖFFNUNGSPLAKATE
                </p>
                <a href="#" class="text-dark mt-5 text-start">
                    FOODFOTOGRAFIE FÜR RESTAURANT-ERÖFFNUNG
                </a>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-5">
                <img src="/images/duktion_fotografie_19.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    DIE STOLZEN SCHÄTZE
                </p>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5">
                <img src="/images/oduktion_fotografie_16.jpg" class="img-fluid">
                <p class="text-secondary text-start mt-3">
                    ALLES IM STRUMPF
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-11 text-center mt-5">
                <hr class="bg-dark">
                <p class="text-dark h2 mt-5">
                    Hunger auf leckere Foodfotografie ? Lass uns sprechen.
                    
                </p>
                <button class="btn btn-outline-dark border border-dark text-muted mt-5"style="border-radius: 0px; height: 70px; width: 200px;">
                        MELDE  DICH  BEI  UNS !
                </button>
            </div>
        </div>
    </div>
</div>
