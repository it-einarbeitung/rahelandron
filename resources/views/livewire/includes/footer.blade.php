<div>
    <div class="bg-dark">
        <div class="container">
            <div class="row mt-5 py-5 my-5">
                <div class="col-lg-1 text-start ">
                    <img src="/images/Logo_RR_white-p-500.png" class="img-fluid" style="width:100px;">
                </div>
                <div class="col-lg-5 text-start mt-4  ms-2">
                    <p class="text-light h1">
                        Rahel & Ron | Food Studio
                    </p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-4 text-start mt-5">
                    <p class="text-light ms-3 h5">
                        SPEAK
                    </p>
                    <a href="#" class="nav-link text-light h5">
                        hello@rahelandron.ch<br>
                        +41 79 961 48 85
                    </a>
                </div>
                <div class="col-lg-4 text-start mt-5">
                    <p class="text-light ms-3 h5">
                        VISIT
                    </p>
                    <a href="#" class="nav-link text-light h5">
                        Denkmalstrasse 17<br>
                        6006 Luzern
                    </a>
                </div>
                <div class="col-lg-4 text-start mt-5">
                    <p class="text-light ms-3 h5">
                        FOLLOW
                    </p>
                    <a href="#" class="nav-link text-light h5">
                        <i class="fab fa-instagram"></i>
                        <i class="fab fa-facebook-f ms-4"></i>
                        <i class="fab fa-pinterest-p ms-4"></i>
                        <i class="fab fa-linkedin-in ms-4"></i>
                    </a>
                </div>
            </div>
            <div class="d-none d-lg-block">
                <div class="row mt-5 py-5">
                    <div class="col-lg-6 mt-5">
                        <p class="text-light text-start">
                            2019 © Copyright Rahel & Ron.<br>
                            All Rights Reserved

                        </p>
                    </div>
                    <div class="col-lg-6 mt-5">
                        <p class="text-light text-end">
                            Von Rahel & Ron gefertigt mit ♥︎<br>
                            Datenschutz | Impressum
                        </p>
                    </div>
                </div>
            </div>
            <div class="d-block d-lg-none">
                <div class="row mt-5 py-5">
                    <div class="col-lg-12 mt-5 text-start">
                        <p class="text-light">
                            2019 © Copyright Rahel & Ron.<br>
                            All Rights Reserved
                        </p>

                        <p class="text-light">
                            Von Rahel & Ron gefertigt mit ♥︎<br>
                            Datenschutz | Impressum
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
