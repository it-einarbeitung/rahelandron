<div>
    <div class="row">
        <div class="col-lg-12">
            <div class="collapse na" id="navbarToggleExternalContent">
                <div class="bg-dark p-4">
                    <img src="/images/2a8c92b1e0_Logo_RR_white.png" class="img-fluid ms-5 mt-5" style="width:60px;"> 
                    <div class="d-flex flex-row-reverse bd-highlight">
                    <button class="btn btn-outline-light border border-light text-muted "style="border-radius: 0px; height: 70px;">
                        LASS  UNS  SPRECHEN
                    </button>
                    </div>  
                </div>
                <div class="bg-dark p-4 text-light">
                   <div class="d-flex flex-column bd-highlight mb-3 ms-5">
                        <div class="p-0 bd-highlight nav-item">
                            <a class="nav-link text-light h3" href="#">
                                <span class="border-start me-3">
                                </span>
                                home
                            </a>
                            <a class="nav-link text-light h3" href="#">
                                über rahel & ron
                            </a>
                            <a class="nav-link text-light h3" href="#">
                                projekte
                            </a>
                            <a class="nav-link text-light h3" href="#">
                                portfolio
                            </a>
                            <a class="nav-link text-light h3" href="#">
                                instagram
                            </a>
                            <a class="nav-link text-light h3" href="#">
                                kontakt
                            </a>
                        </div>
                        <div class="p-0 bd-highlight text-light nav-item">
                            <hr class="  border border-light borer border-0 bg-light" style="width:600px;">
                             
                             <a class="nav-link text-light h3" href="#">
                                Limpressum
                             </a>
                             <a class="nav-link text-light h3" href="#">
                                datenschutz
                             </a>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 text-start mt-5 ">
                                <p class="text-light ms-3 h5">
                                    FOLLOW
                                </p>
                                <a href="#" class="nav-link text-light h5">
                                    <i class="fab fa-instagram"></i>
                                    <i class="fab fa-facebook-f ms-4"></i>
                                    <i class="fab fa-pinterest-p ms-4"></i>
                                    <i class="fab fa-linkedin-in ms-4"></i>
                                </a>
                            </div>  
                            <div class="col-lg-6 text-start mt-5">
                                <p class="text-light ms-3 h5">
                                    SPEAK
                                </p>
                                <a href="#" class="nav-link text-light h5">
                                    hello@rahelandron.ch<br>
                                    +41 79 961 48 85
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-light bg-light mt-5">
                <div class="container-fluid ">
                    <img src="/images/5c44_Logo_RR_black-p-500.png" class="img-fluid ms-5" style="width:80px;">
                    <button class="navbar-toggler btn btn-outline-dark ms-auto text-muted me-3" style="border-radius: 0px; height: 70px;">
                         LASS  UNS  SPRECHEN
                    </button>
                    <button class="navbar-toggler border border-light me-5 " type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">
                        </span>
                    </button>
                </div>
            </nav>   
        </div>
    </div>
</div>
