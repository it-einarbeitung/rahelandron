<div class="container">
   <div class="row mt-5">
        <div class="col-lg-12 text-center">
            <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active" data-bs-interval="10000">
                        <img src="/images/49_heimatli-rahelandron-foodfotografie-00750-p-1600.jpg" class=" d-block w-100" alt="...">
                        <div class="row">
                            <div class="col-lg-9"></div>
                            <div class="col-lg-3 text-center">
                                <div class="card bg-dark"style="border-radius:0px; margin-top: -600px;">
                                    <div class="card-body text-light h4">
                                        <b>Content - Creation</b>
                                    </div>
                                </div>
                                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px;">
                                    <div class="card-body text-light h4">
                                        <b>&  leckere Food</b>
                                    </div>
                                </div>
                                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px;">
                                    <div class="card-body text-light h4">
                                        <b>Fotografie</b>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                    <div class="carousel-item" data-bs-interval="2000">
                        <img src="/images/6067094273adcbbc451cb707_uondas-rahelandron-foodfotografie-00110-p-1600.jpg" class="d-block w-100" alt="...">
                        <div class="row">
                            <div class="col-lg-9"></div>
                            <div class="col-lg-3 text-center">
                                <div class="card bg-dark"style="border-radius:0px; margin-top: -600px;">
                                    <div class="card-body text-light h4">
                                        <b>Foodfotografie</b>
                                    </div>
                                </div>
                                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px;">
                                    <div class="card-body text-light h4">
                                        <b>&  Foodstyling</b>
                                    </div>
                                </div>
                                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px;">
                                    <div class="card-body text-light h4">
                                        <b>Von Rahel & Ron</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="/images/5c5e092a364f64816ef53ff3_rahelandron_restaurant_schlüssel_restaurantfotografie_3.jpg" class="d-block w-100" alt="...">
                        <div class="row">
                            <div class="col-lg-9"></div>
                            <div class="col-lg-3 text-center">
                                <div class="card bg-dark"style="border-radius:0px; margin-top: -600px;">
                                    <div class="card-body text-light h4">
                                        <b>Foodfotografie</b>
                                    </div>
                                </div>
                                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px;">
                                    <div class="card-body text-light h4">
                                        <b>&  Foodstyling</b>
                                    </div>
                                </div>
                                <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px;">
                                    <div class="card-body text-light h4">
                                        <b>Von Rahel & Ron</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                    </span>
                    <span class="visually-hidden">
                        Previous
                    </span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true">
                    </span>
                    <span class="visually-hidden">
                        Next
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <p class="text-dark text-center h2">
                Hallo, wir sind Rahel & Ron.
            </p>
            <p class="text-dark text-center h2 mt-3">
                Unser Food Studio steht für spannende Foodfotografie,<br>
                schmackhaftes Foodstyling,  visuelles Storytelling und<br>
                leidenschaftliches Kochen.  Hunger auf leckere Fotos?
            </p>
            <p class="text-dark text-center h1">
                .  .  .
            </p>
            <p class="text-dark text-center h2 mt-3">
                Liebe Grüsse,
            </p>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-5"></div>
        <div class="col-lg-2 mt-5">
            <img src="/images/5c446d3379dc6925e9b4d94d_5b4f91c25d042125fb21dcaf_rahelandron_signature-p-500.png" class="img-fluid">
        </div>
        <div class="col-lg-5"></div>
    </div>
    <div class="row">
        <div class="col-lg-4 text-center">
            <img src="/images/rahelandron.ch-p-800.png"class="img-fluid">
            <p class="text-dark text-start h5 mt-5">
                <b>RAHEL RÜTTIMANN</b>
            </p>
            <p class="text-secondary text-start">
                STYLISTIN & CHEF DE CUISINE
            </p>
        </div>
        <div class="col-lg-4 text-center">
            <img src="/images/rahelandron.ch-1-p-800.jpg"class="img-fluid">
            <p class="text-dark text-start h5 mt-5">
                <b>SANNY</b>
            </p>
            <p class="text-secondary text-start">
                MOTIVATION MANAGER
            </p>
        </div>
        <div class="col-lg-4 text-center">
            <img src="/images/rahelandron-2-.ch-p-800.png"class="img-fluid">
            <p class="text-dark text-start h5 mt-5">
                <b>RON EDWARDS</b>
            </p>
            <p class="text-secondary text-start">
                FOTOGRAF & ONLINE BERATER
            </p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-9"></div>
        <div class="col-lg-2 text-end mt-5">
            <div class="card bg-dark text-center"style="border-radius: 0px;">
                <div class="card-body text-light h2">
                    <b>LETZTE</b>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8"></div>
        <div class="col-lg-3 mt-3">
            <div class="card bg-dark text-center"style="border-radius: 0px;">
                <div class="card-body text-light h2">
                    <b>FOTOGRAFIE-</b>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-9"></div>
        <div class="col-lg-2 text-end">
            <div class="card bg-dark text-center"style="border-radius: 0px;">
                <div class="card-body text-light h2">
                    <b>PROJEKTE</b>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-6 text-center mt-5">
            <img src="/images/ueli-hof-produkt-fotografie-rahelandron-natur-02778-p-1600.jpg" class="img-fluid">
            <p class="text-dark h3 text-start" style="margin-top: -30px;">
                <b class="text-light">Key Visuals für neue</b> <br>
                    <b>Verpackungen</b>  
            </p>
            <p class="text-secondary text-start">
                UELI-HOF BIO-METZGEREI
            </p>
        </div>
        <div class="col-lg-6 text-center ">
            <img src="/images/uh-baerlauchwurst-packshot_hurrah_luzern_08-p-1600.jpg" class="img-fluid">
            <p class="text-dark h3 text-start" style="margin-top: -30px;">
                <b>Produkt Fotografie<br> 
                    für Bio-Metzgerei</b>  
            </p>
            <p class="text-secondary text-start">
                UELI-HOF BIO-METZGEREI
            </p>
        </div>   
    </div>
    <div class="row mt-5">
        <div class="col-lg-6 text-center">
            <img src="/images/49_heimatli-rahelandron-foodfotografie-00750-p-1600.jpg" class="img-fluid">
            <p class="text-dark h3 text-start" style="margin-top: -30px;">
                <b class="text-light">Food Fotografie<br></b>
                    <b>für's «Heimatli»</b>  
            </p>
            <p class="text-secondary text-start">
                CHESA ROSATSCH
            </p>
        </div>
        <div class="col-lg-6 text-center">
            <img src="/images/6067094273adcbbc451cb707_uondas-rahelandron-foodfotografie-00110-p-1600.jpg" class="img-fluid">
            <p class="text-dark h3 text-start" style="margin-top: -30px;">
                <b class="text-dark">
                    <b class="text-light">Food Fotografie für<br></b>
                    <b>das «Uondas»</b>  
            </p>
            <p class="text-secondary text-start">
                CHESA ROSATSCH CELERINA
            </p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-4 text-center">
            <img src="/images/die-hühnerei-rahelandron-foodfotografie-01089.jpg" class="img-fluid">
            <p class="text-dark h3 text-start" style="margin-top: -30px;">
                <b class="text-light">Food Fotografie<br></b>
                    <b>Die «Hühnerei»</b>  
            </p>
            <p class="text-secondary text-start">
                CHESA ROSATSCH
            </p>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-4 text-center">
            <img src="/images/rahelandron_restaurant_schlüssel_foodfotografie_restaurantfotografie_14.jpg" class="img-fluid">
            <p class="text-dark h3 text-start" style="margin-top: -30px;">
                <b class="text-dark">
                    <b class="text-light">Food Fotografie für<br></b>
                    <b>Sterne Menü</b>  
            </p>
            <p class="text-secondary text-start">
                CHESA ROSATSCH CELERINA
            </p>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <img src="/images/rahelandron-feuerring-08607-p-2000.jpg" class="img-fluid">
            <a href="#" class="nav-link text-dark h1 mt-5">
               ZU WEITEREN KUNDENPROJEKTEN<br>
                <i class="fas fa-forward"></i>
            </a>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <hr class="text-dark">
                <p class="text-dark text-center h1 mt-5">
                    Hunger auf leckere Foodfotografie? Lass uns sprechen.
                </p>
                <button class="btn btn-outline-dark border border-dark text-muted text-dark h4 mt-5"style="border-radius: 0px; height: 70px;">
                    MELDE DICH BEI UNS!
                </button>
            </div>
        </div>
    </div>
</div>
