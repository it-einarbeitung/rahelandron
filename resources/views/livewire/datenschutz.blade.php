<div>
    <div class="container">
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-12 mt-5">
                    <img src="/images/odfotografie_portfolio_5.jpg" class="img-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-3 text-center">
                            <div class="card bg-dark"style="border-radius:0px; margin-top: -600px; margin-right: -40px;">
                                <div class="card-body text-light h4">
                                    <b>Impressum</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right: -40px;">
                                <div class="card-body text-light h4">
                                    <b>& Rechtliches</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-12 mt-5">
                    <img src="/images/odfotografie_portfolio_5.jpg" class="img-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-2 text-center">
                            <div class="card bg-dark"style="border-radius:0px; margin-top: -0px; margin-right: -px;">
                                <div class="card-body text-light h4">
                                    <b>Datenschutz</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right: -px;">
                                <div class="card-body text-light h4">
                                    <b>von Rahel & Ron</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1"></div>
            <div class="col-lg-11 text-start mt-5">
                <p class="text-dark h1 mt-5">
                    DATENSCHUTZERKLÄRUNG
                </p>
                <p class="text-dark h5 mt-5">
                   Gestützt auf Artikel 13 der schweizerischen Bundesverfassung und die datenschutzrechtlichen Bestimmungen des Bundes  
                </p>
                <p class="text-dark h5">
                   (Datenschutzgesetz, DSG) hat jede Person Anspruch auf Schutz ihrer Privatsphäre sowie auf Schutz vor Missbrauch ihrer  
                </p>
                <p class="text-dark h5">
                   persönlichen Daten. Wir halten diese Bestimmungen ein. Persönliche Daten werden streng vertraulich behandelt und weder an  
                </p>
                <p class="text-dark h5">
                    Dritte verkauft noch weiter gegeben.
                </p>
                <p class="text-dark h5 mt-4">
                    In enger Zusammenarbeit mit unseren Hosting-Providern bemühen wir uns, die Datenbanken so gut wie möglich vor fremden 
                </p>
                <p class="text-dark h5">
                    Zugriffen, Verlusten, Missbrauch oder vor Fälschung zu schützen.
                </p>
                <p class="text-dark h5">
                    Beim Zugriff auf unsere Webseiten werden folgende Daten in Logfiles gespeichert: IP-Adresse, Datum, Uhrzeit, Browser-
                </p>
                <p class="text-dark h5">
                    Anfrage und allg. übertragene Informationen zum Betriebssystem resp. Browser. Diese Nutzungsdaten bilden die Basis für 
                </p>
                <p class="text-dark h5">
                    statistische, anonyme Auswertungen, so dass Trends erkennbar sind, anhand derer wir unsere Angebote entsprechend 
                </p>
                <p class="text-dark h5">
                    verbessern können.
                </p>
                <p class="text-dark h1 mt-5">
                    DATENSCHUTZERKLÄRUNG FÜR DIE NUTZUNG VON FACEBOOK-PLUGINS (LIKE-BUTTON)
                </p>
                <p class="text-dark h5 mt-4">
                    Auf unseren Seiten sind Plugins des sozialen Netzwerks Facebook, 1601 South California Avenue, Palo Alto, CA 94304, USA 
                </p>
                <p class="text-dark h5">
                    integriert. Die Facebook-Plugins erkennen Sie an dem Facebook-Logo oder dem “Like-Button” (“Gefällt mir”) auf unserer 
                </p>
                <p class="text-dark h5">
                    Seite. 
                </p>
                <p class="text-dark h5 mt-4">
                   Eine Übersicht über die Facebook-Plugins finden Sie hier: https://developers.facebook.com/ 
                </p>
                <p class="text-dark h5">
                    docs/plugins/.
                </p>
                <p class="text-dark h5 mt-4">
                    Wenn Sie unsere Seiten besuchen, wird über das Plugin eine direkte Verbindung zwischen Ihrem Browser und dem Facebook-
                </p>
                <p class="text-dark h5">
                   Server hergestellt. Facebook erhält dadurch die Information, dass Sie mit Ihrer IP-Adresse unsere Seite besucht haben. Wenn  
                </p>
                <p class="text-dark h5">
                    Sie den Facebook “Like-Button” anklicken während Sie in Ihrem Facebook-Account eingeloggt sind, können Sie die Inhalte 
                </p>
                <p class="text-dark h5">
                    unserer Seiten auf Ihrem Facebook-Profil verlinken. Dadurch kann Facebook den Besuch unserer Seiten Ihrem Benutzerkonto 
                </p>
                <p class="text-dark h5">
                    zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie 
                </p>
                <p class="text-dark h5">
                    deren Nutzung durch Facebook erhalten.
                </p>
                <p class="text-dark h5 mt-4">
                    Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von facebook unter: https://www.facebook.com/
                </p>
                <p class="text-dark h5">
                    about/privacy/
                </p>
                <p class="text-dark h5 mt-4">
                    Wenn Sie nicht wünschen, dass Facebook den Besuch unserer Seiten Ihrem Facebook-Nutzerkonto zuordnen kann, loggen Sie sich bitte aus Ihrem Facebook-Benutzerkonto aus.
                </p>
                <p class="text-dark h1 mt-5">
                    DATENSCHUTZERKLÄRUNG FÜR DIE NUTZUNG VON GOOGLE ANALYTICS
                </p>
                <p class="text-dark h5 mt-4">
                   Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (“Google”). Google Analytics verwendet sog. 
                </p>
                <p class="text-dark h5">
                   “Cookies”, Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch 
                </p>
                <p class="text-dark h5">
                   Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an 
                </p>
                <p class="text-dark h5">
                   einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf 
                </p>
                <p class="text-dark h5">
                   dieser Webseite wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in 
                </p>
                <p class="text-dark h5">
                   anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt.
                </p>
                <p class="text-dark h5 mt-4">
                   Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Google 
                </p>
                <p class="text-dark h5">
                   wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die 
                </p>
                <p class="text-dark h5">
                   Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene 
                </p>
                <p class="text-dark h5">
                   Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies 
                </p>
                <p class="text-dark h5">
                   gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Die im Rahmen von Google 
                </p>
                <p class="text-dark h5">
                   Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.
                </p>
                <p class="text-dark h5 mt-4">
                    Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen 
                </p>
                <p class="text-dark h5">
                   Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen  
                </p>
                <p class="text-dark h5">
                    können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google 
                </p>
                <p class="text-dark h5">
                    in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.
                </p>
            </div>
        </div>
    </div>
</div>
