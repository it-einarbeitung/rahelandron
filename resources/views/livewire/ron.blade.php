<div>
    <div class="container">
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-12 text-center mt-5">
                    <img src="/images/oodfotografie_portfolio_11.jpg" class="img-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-3 text-center">
                            <div class="card bg-dark"style="border-radius:0px; margin-top: -700px; margin-right: -40px;">
                                <div class="card-body text-light h4">
                                        <b>Foodfotografie</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right: -40px;">
                                <div class="card-body text-light h4">
                                    <b>&  leckere Food</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px; margin-right:-40px;">
                                <div class="card-body text-light h4">
                                    <b>von Rahel & Ron</b>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-12 text-center mt-5">
                    <img src="/images/oodfotografie_portfolio_11.jpg" class="img-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-2 text-center" style="width: 166px; ">
                            <div class="card bg-dark"style="border-radius:0px; margin-top: -210px;">
                                <div class="card-body text-light">
                                        <b>Foodfotografie</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px;">
                                <div class="card-body text-light">
                                    <b>&  leckere Food</b>
                                </div>
                            </div>
                            <div class="card bg-dark mt-3"style="border-radius:0px; margin-top: 0px;">
                                <div class="card-body text-light">
                                    <b>von Rahel & Ron</b>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1"></div>
            <div class="col-lg-11 text-center mt-5">
                <p class="text-dark h1 text-start mt-5">
                    Wir sind ein kleines aber feines Food Studio im Herzen
                </p>
                <p class="text-dark h1 text-start">
                    der Schweiz, welches sich spannender Foodfotografie
                </p>
                <p class="text-dark h1 text-start">
                    und geschmackvollem Foodstyling gewidmet hat.
                </p>
                <p class="text-dark h1 text-start mt-4">
                    Die Liebe für gutes Essen, Aestehtik im Styling von 
                </p>
                <p class="text-dark h1 text-start">
                    Food und moderne Fotografie hat uns bewogen dieses 
                </p>
                <p class="text-dark h1 text-start">
                    FAbenteuer einzugehen – Hunger auf gute Bilder?
                </p>
                    <button class="btn btn-outline-dark border border-dark text-muted mt-5"style="border-radius: 0px; height: 70px; width: 200px;">
                        LASS  UNS  SPRECHEN
                    </button>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 text-start mt-5">
              <p class="text-dark h1">
                  Das sind wir
              </p>
              <hr class="bg-dark">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 text-center">
                <img src="/images/rahelandron.ch-p-800.png"class="img-fluid">
                    <p class="text-dark text-start h5 mt-5">
                        <b>RAHEL RÜTTIMANN</b>
                    </p>
                    <p class="text-secondary text-start">
                        STYLISTIN & CHEF DE CUISINE
                    </p>
                    <p class="text-dark h5 text-start mt-5">
                        Rahel hat als Dekorationsgestalterin das 
                    </p>
                    <p class="text-dark h5 text-start">
                        Händchen für schöne Sachen. Bei uns ist sie  
                    </p>
                    <p class="text-dark h5 text-start">
                        zuständig für die Inszenierung der Sets und somit   
                    </p>
                    <p class="text-dark h5 text-start">
                        verantwortlich für's Food-Styling.  
                    </p>
                    <p class="text-dark h5 text-start mt-4">
                        Dazu ist sie auch die treibende Kraft in der   
                    </p>
                    <p class="text-dark h5 text-start">
                        Küche, denn ihre Kreativität lebt sie beim Kochen   
                    </p>
                    <p class="text-dark h5 text-start">
                        aus.Als unser Creative Director saugt sie sich die    
                    </p>
                    <p class="text-dark h5 text-start">
                        Ideen aus den Fingern, welche wir im Team realisieren.   
                    </p>
            </div>
            <div class="col-lg-4 text-center">
                <img src="/images/rahelandron.ch-1-p-800.jpg"class="img-fluid">
                <p class="text-dark text-start h5 mt-5">
                    <b>SANNY</b>
                </p>
                <p class="text-secondary text-start">
                    MOTIVATION MANAGER
                </p>
                <p class="text-dark h5 text-start mt-5">
                    Sanny ist seit nun fast 10 Jahren, damit  
                </p>
                <p class="text-dark h5 text-start">
                    beschäftigt uns zu motivieren. Mit ihrem lauten  
                </p>
                <p class="text-dark h5 text-start">
                    Miauen und noch lauterem Schnurren hat sie   
                </p>
                <p class="text-dark h5 text-start">
                    dies ziemlich gut im Griff.   
                </p>
            </div>
            <div class="col-lg-4 text-center">
                <img src="/images/rahelandron-2-.ch-p-800.png"class="img-fluid">
                <p class="text-dark text-start h5 mt-5">
                    <b>RON EDWARDS</b>
                </p>
                <p class="text-secondary text-start">
                    FOTOGRAF & ONLINE BERATER
                </p>
                <p class="text-dark h5 text-start mt-5">
                    Mit dem Blick übers Ganze und dem Auge fürs  
                </p>
                <p class="text-dark h5 text-start">
                    Detail hat Ron die Passion für die Fotografie    
                </p>
                <p class="text-dark h5 text-start">
                    entdeckt. Da er dazu auch noch leidenschaftlich     
                </p>
                <p class="text-dark h5 text-start">
                    gerne kocht und isst, ist es nicht verwunderlich,     
                </p>
                <p class="text-dark h5 text-start">
                    dass er zurFood-Fotografie gefunden hat.     
                </p>
                <p class="text-dark h5 text-start mt-4">
                    Mit Erfahrungen im Verkauf und Marketing, 
                </p>
                <p class="text-dark h5 text-start">
                    unterstützt er unsere Kunden die richtige      
                </p>
                <p class="text-dark h5 text-start">
                    Zielgruppe online zu erreichen. Während seiner       
                </p>
                <p class="text-dark h5 text-start">
                    Arbeit in verschiedenen Agenturen konnte er       
                </p>
                <p class="text-dark h5 text-start">
                    Wissen über Digital Marketing aufbauen, um       
                </p>
                <p class="text-dark h5 text-start">
                    dieses an unsere Kunden weiterzugeben.       
                </p>
            </div>
        </div>
    </div>
    <div class="bg-dark py-5">
        <div class="container">
        <div class="row">
            
            
            <div class="col-lg-12 text-start">
                <p class="text-light h1 mt-5">
                    Unsere Dienstleistungen
                </p>
                <hr class="bg-light">
            </div>
        </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <p class="text-light h3 text-start mt-5">
                        food fotografie
                    </p>
                    <p class="text-light h5 text-start mt-5">
                        Hunger? Hunger nach leckerer Fotografie? Wir 
                    </p>
                    <p class="text-light h5 text-start">
                        haben da genau das richtige für dich. Deine  
                    </p>
                    <p class="text-light h5 text-start">
                        Produkte schmackhaft inszeniert und genau im  
                    </p>
                    <p class="text-light h5 text-start">
                       richtigen Licht präsentiert. Food-Fotografie, die   
                    </p>
                    <p class="text-light h5 text-start">
                        deine Story erzählt.    
                    </p>
                    <p class="text-light h5 text-start mt-4">
                        Was meinst du?Ist es Zeit dich,dein Produkt oder     
                    </p>
                    <p class="text-light h5 text-start">
                        Wdein Restaurant zum Star zu machen?     
                    </p>
                </div>
                <div class="col-lg-4">
                    <p class="text-light h3 text-start mt-5">
                        food content
                    </p>
                    <p class="text-light h5 text-start mt-5">
                        Kochen ist nicht nur unsere Leidenschaft, sondern  
                    </p>
                    <p class="text-light h5 text-start">
                        wir erzählen gerne Geschichten zu Gerichten,   
                    </p>
                    <p class="text-light h5 text-start">
                        Zutaten und Personen.Zu jedem Essen kann man   
                    </p>
                    <p class="text-light h5 text-start">
                       eine spannende Geschichte erzählen   
                    </p>
                    <p class="text-light h5 text-start mt-4">
                        Lass uns zusammen deine Story, die deines      
                    </p>
                    <p class="text-light h5 text-start">
                        Produktes oder deiner Dienstleistung erzählen –      
                    </p>
                    <p class="text-light h5 text-start">
                        offene Ohren kommen dann von selbst!      
                    </p>
                </div>
                <div class="col-lg-4">
                    <p class="text-light h3 text-start mt-5">
                        social media & search
                    </p>
                    <p class="text-light h5 text-start mt-5">
                        Social-Media ist nicht nur weitreichend, sonder  
                    </p>
                    <p class="text-light h5 text-start">
                       kann auch so einfach sein. Man muss es aber zu    
                    </p>
                    <p class="text-light h5 text-start">
                        nutzen wissen – wir wissen wie und unterstützen   
                    </p>
                    <p class="text-light h5 text-start">
                       dich dabei.   
                    </p>
                    <p class="text-light h5 text-start mt-4">
                       Wir kreieren mit dir deinen Content, erzählen deine      
                    </p>
                    <p class="text-light h5 text-start">
                        Geschichte und erreichen deine Zielgruppe – auf      
                    </p>
                    <p class="text-light h5 text-start">
                        Instagram, Facebook und auch Google.       
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row mt-5">
            <div class="col-lg-12 text-center">
                <hr class="bg-dark mt-5">
                <p class="text-dark h1 mt-5">
                    Hunger auf leckere Foodfotografie ? Lass uns sprechen .
                </p>
                <button class="btn btn-outline-dark border border-dark text-muted mt-5"style="border-radius: 0px; height: 70px; width: 200px;">
                        MELDE DICH  BEI UNS !
                </button>
            </div>
        </div>
    </div>
</div>



