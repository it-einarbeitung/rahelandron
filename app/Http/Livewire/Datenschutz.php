<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Datenschutz extends Component
{
    public function render()
    {
        return view('livewire.datenschutz');
    }
}
