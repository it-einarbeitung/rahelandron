<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Impressum extends Component
{
    public function render()
    {
        return view('livewire.impressum');
    }
}
